# Grid coalescent

A notebook that computes realizations of the coalescent on an Aevol-like grid, with one individual per cell and no selection.
